<?php 
include_once '../model/Grafo.class.php';
include_once '../model/Vertice.class.php';
include_once '../model/Aresta.class.php';

//controle de erros
error_reporting(E_ALL);
ini_set('display_errors', '1');

//limite de tempo para gerar o resultado
set_time_limit(10000);

//objeto grafo
$grafo = new Grafo(); 

//recebe a matriz e o tipo de busca a ser realizada
if($_REQUEST['acao'] == "busca")
{
    //converte o valor da direcao para positivo 
    $direcao = -1*$_REQUEST["direcao"]; 

    if ($direcao < 0) 
    {
        $direcao = $direcao + 360;
    }
    //adiciona a direcao inicial ao grafo
    $grafo->setDirecao($direcao);

    //recebe a matriz
    $matriz = json_decode($_REQUEST["json"]);

    //converte a matriz para o grafo e retorna posições de início e fim
    $posicoes = $grafo->converterMatrizParaGrafo($matriz);

    //marca as posições
    $inicio = $posicoes[0];
    $fim = $posicoes[1];

    //calcula tempo inicial
    $time_start = microtime(true);

    //se for busca em profundidade
    if($_REQUEST["escolha"] == 'P')
    {
        $grafo->DFSNormal($inicio, $fim); 
    }
    //se for A estrela
    else if ($_REQUEST["escolha"] == 'A')
    {
        $grafo->A_estrela($inicio, $fim); 
    }
    else if($_REQUEST["escolha"] == 'D')
    {
        $grafo->Dijkstra($inicio, $fim); 
    }
    else if($_REQUEST["escolha"] == 'L')
    {
        $grafo->BFSNormal($inicio, $fim); 
    }
    else
    {
        echo "PROBLEMA ESCOLHA";
    }

    //calcula tempo final
    $time_end = microtime(true);
    $time_execution = ($time_end - $time_start); 

    //percorre os vertices e retorna o caminho entre início e fim 
    $caminho = $grafo->encontra_Caminho($inicio, $fim, []); 

    //obtém todos os vertices que foram visitados
    $visitados = $grafo->getVerticesVisitados();

    //obtém objeto do vértice fim
    $vertice_final = $grafo->getVertice($fim);

    //obtém o custo para chegar no vértice final
    $custo_total = $vertice_final->getCusto();

    //trata as infos para envio
    $resultado = [];
    $resultado["visitados"] = $visitados;
    $resultado["caminho"] = $caminho; 
    $resultado["qtdVisitados"] = sizeof($visitados);
    $resultado["qtdCaminho"] = sizeof($caminho);
    $resultado["tempo"] = number_format($time_execution, 5); 
    $resultado["custo"] = $custo_total;

    //emite as infos na forma de json
    echo json_encode($resultado,JSON_FORCE_OBJECT);
}
//receber os parâmetros da index para gerar a tabela
if($_REQUEST['acao'] == "GerarTabela")
{
    //tamanho
    $largura = urldecode($_REQUEST['largura']);
    $altura = urldecode($_REQUEST['altura']);

    //gera a tabela
    $html = '<table id="id_tabela">'; 

    //percorrendo as posicoes 
    for ($i=0; $i < $altura; $i++) 
    { 
    $html .= "<tr>";
        for ($j=0; $j < $largura ; $j++) 
        { 
            $html .= "<td>";
            // $html .= $i*$largura + $j + 1;
            $html .= "</td>";
        }
        $html .= "</tr>";
    }
    $html .= ' </table>';

    //emite a tabela
    echo $html;
}
?>