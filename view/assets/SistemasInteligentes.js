var estados = new function(){
    this.isAgente = false;
    this.isDestino = false;
    this.largura = 0;
    this.altura = 0; 
    this.direcao = 0;
    this.acao = 0;
    this.controle = false;
}
$(document).ready(function() 
{
    $("#largura").val(10); 
    $("#altura").val(10); 
    gerarTabela();

    $("#tabela").on('mouseup', 'td',  function(e){
        estados.controle = false;
        console.log("mouseup");
    });
    
    $("#tabela").on('mousedown', 'td', function(e){
        estados.controle = true;
        controleElementos(this);
        console.log("mousedown");
    });

    $("#tabela").on('mousemove', 'td', function(e){
        console.log("mousemove");
        if(estados.controle)
        {
            controleElementos(this);
        }
    });

    $('#antihorario').click(function(){
        rotateImage(-45);
    });
    $('#horario').click(function(){
        rotateImage(45);
    });
    $('button').click(function(){

        var id = $(this).attr('id');

        if(id == 'btn-green')
        {
            $("#btn-green").css("border-color", "black");
            $("#btn-red").css("border-color", "rgb(216, 213, 213)");
            $("#btn-black").css("border-color", "rgb(216, 213, 213)");
            $("#btn-white").css("border-color", "rgb(216, 213, 213)");
            estados.acao = 0;
        }
        if(id == 'btn-red')
        {
            $("#btn-green").css("border-color", "rgb(216, 213, 213)");
            $("#btn-red").css("border-color", "black");
            $("#btn-black").css("border-color", "rgb(216, 213, 213)");
            $("#btn-white").css("border-color", "rgb(216, 213, 213)");
            estados.acao = 1;
        }
        if(id == 'btn-black')
        {
            $("#btn-green").css("border-color", "rgb(216, 213, 213)");
            $("#btn-red").css("border-color", "rgb(216, 213, 213)");
            $("#btn-black").css("border-color", "black");
            $("#btn-white").css("border-color", "rgb(216, 213, 213)");
            estados.acao = 2;
        }
        if(id == 'btn-white')
        {
            $("#btn-green").css("border-color", "rgb(216, 213, 213)");
            $("#btn-red").css("border-color", "rgb(216, 213, 213)");
            $("#btn-black").css("border-color", "rgb(216, 213, 213)");
            $("#btn-white").css("border-color", "black");
            estados.acao = 3;
        }
    });
});
function rotateImage(degree) 
{
    estados.direcao = estados.direcao + degree; 
    if(estados.direcao == 360 || estados.direcao == -360)
    {
        estados.direcao = 0;
    }
    console.log(estados.direcao);
    $('#image').css({
        '-webkit-transform': 'rotate('+estados.direcao+'deg)',
        '-moz-transform': 'rotate('+estados.direcao+'deg)'
    });
    return false;
}

function createArray(length) 
{
    var arr = new Array(length || 0),
        i = length;

    if (arguments.length > 1) 
    {
        var args = Array.prototype.slice.call(arguments, 1);
        while(i--) arr[length-1 - i] = createArray.apply(this, args);
    }

    return arr;
}
function busca(escolha)
{
    var matriz = createArray(estados.altura, estados.largura); 
    var i = 0;
    $('#id_tabela').find('tr').each(function (i, el) {

        var $tds = $(this).find('td') 
        for (let j = 0; j < $tds.length; j++) 
        {
            var $td = $tds.eq(j);
            var cor = $td.css("background-color");

            var verde = 0;
            var vermelho = 1;
            var preto = 2;
            var branco = 3;
            
            //verde
            if(cor == 'rgb(0, 128, 0)')
            {
                matriz[i][j] = verde;
            }
            //vermelho
            else if(cor== 'rgb(255, 0, 0)')
            {
                matriz[i][j] = vermelho;
            }
            //preto
            else if(cor== 'rgb(0, 0, 0)')
            {
                matriz[i][j] = preto;
            }
            //branco
            else if(cor== 'rgb(255, 255, 255)' || 'rgba(0, 0, 0, 0)')
            {
                matriz[i][j] = branco;
            }
            else 
            {
                matriz[i][j] = -1;
            }
        }
        i++; 
    });

    $.ajax({
        url: "controller/SistemasInteligentes.ajax.php?acao=busca&escolha=" + escolha + "&direcao=" + estados.direcao,
        type: 'GET',
        dataType: 'JSON',
        contentType: "application/json; charset=utf-8",
        success: function (html) {
            let json = html;
            let i = 0;
            $('#id_tabela').find('tr').each(function (i, el) {
                var $tds = $(this).find('td') 
                for (let j = 0; j < $tds.length; j++) 
                {
                    var $td = $tds.eq(j);
                    var posicao = i*estados.largura + j + 1; 
                    var cor = $td.css("background-color");

                    //  alert(cor);

                    if(cor != 'rgb(255, 0, 0)' && cor != 'rgb(0, 128, 0)' && cor != 'rgb(0, 0, 0)')
                    {
                        $td.css("background", "rgb(247, 241, 241)");
                    }
                    for(let k in json.visitados)
                    {
                        if(posicao == json.visitados[k] && cor != 'rgb(255, 0, 0)' && cor != 'rgb(0, 128, 0)' && cor != 'rgb(0, 0, 0)')
                        {
                            $td.css("background", "lightblue");
                        }
                    }
                    for(let k in json.caminho)
                    {
                        if(posicao == json.caminho[k] && cor != 'rgb(255, 0, 0)' && cor != 'rgb(0, 128, 0)' && cor != 'rgb(0, 0, 0)')
                        {
                            $td.css("background", "gray");
                        }
                    }
                }
                i++; 
            });
            $("#result").html( "Tempo(s) = " + json.tempo + " - Visitados: " + json.qtdVisitados + " - Caminho: " + json.qtdCaminho + " - Custo: " + json.custo);
        },
        data: "json="+ JSON.stringify(matriz) 
    });
}
function gerarTabela()
{
    var largura = encodeURIComponent(document.getElementById("largura").value);
    var altura = encodeURIComponent(document.getElementById("altura").value);

    // alert(largura);

    $.ajax({
    type: "GET",
    url: "controller/SistemasInteligentes.ajax.php",
    data: "acao=GerarTabela&largura=" + largura + "&altura=" + altura,
    cache: false,
    success: function(html){
        // alert(html);
        $("#tabela").html(html);
        estados.isAgente = false;
        estados.isDestino = false;
        estados.largura = largura;
        estados.altura = altura;
        }
    });
}
function limparTabela()
{
    $('#id_tabela').find('tr').each(function (i, el) {
        var $tds = $(this).find('td') 
        for (let j = 0; j < $tds.length; j++) 
        {
            var $td = $tds.eq(j);
            var posicao = i*estados.largura + j + 1; 
            $td.css("background", "white");
            $td.closest("img").prev().empty(); 
        }
        i++; 
    });
    estados.isAgente = false;
    estados.isDestino = false;
}
function preencherTabela(cor)
{
    $('#id_tabela').find('tr').each(function (i, el) {
        var $tds = $(this).find('td') 
        for (let j = 0; j < $tds.length; j++) 
        {
            var $td = $tds.eq(j);
            var posicao = i*estados.largura + j + 1; 
            $td.css("background", cor);
            $td.html(""); 
        }
        i++; 
    });
    estados.isAgente = false;
    estados.isDestino = false;
}
function controleElementos(td)
{
    var column_num = parseInt( $(td).index() ) + 1;
    var row_num = parseInt( $(td).parent().index())+1;
    var acao = estados.acao;
    console.log(acao);
    if($(td).css("background-color") == "rgb(0, 128, 0)")
    {
        estados.isAgente = false;  
        $(td).html(""); 
    }

    if($(td).css("background-color") == "rgb(255, 0, 0)")
    {
        estados.isDestino = false;
        $(td).html(""); 
    }
    if(acao == 0)
    {
        if(estados.isAgente == false)
        {
            $(td).html("<img id=\"image\" src=\"view/img/arrow.png\" width=\"20\" height=\"20\"></img>");
            $(td).css("background-color", "green");
            estados.isAgente = true;
        }
    }
    else if (acao == 1)
    {
        if(!estados.isDestino)
        {
            $(td).html("<img  src=\"view/img/x.png\" width=\"20\" height=\"20\"></img>");
            $(td).css("background-color", "red");
            estados.isDestino = true;
        }
    }
    else if (acao == 2)
    {
        $(td).css("background-color", "black");
    }
    else if(acao == 3)
    {
        if($(td).css("background-color") == "rgb(247, 241, 241)")
        {
            return;
        }
        else
        {
            $(td).css("background-color", "rgb(247, 241, 241)");
        }
    }
}
