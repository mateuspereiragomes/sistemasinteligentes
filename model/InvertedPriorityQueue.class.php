<?php
//fila de prioridade invertida
class InvertedPriorityQueue extends SplPriorityQueue
{
    public function compare($priority1, $priority2)
    {
        if ($priority1 === $priority2) return 0;
        
        //inverte a prioridade
        return $priority1 < $priority2 ? 1 : -1;
    }
} 
?>