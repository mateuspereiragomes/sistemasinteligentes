<?php
include_once 'Vertice.class.php';
include_once 'Grafo.class.php';

//classe que contém as propriedades da aresta entre dois vértices
class Aresta
{   
    private $origem;
    private $destino;   
    private $peso;
    private $direcao; 

    public function __construct($o, $d, $p = 0)
    {
        $this->origem = $o;
        $this->destino = $d;
        $this->peso = $p; 
        $this->direcao = false; 
    }
    public function __destruct()
    {
        $texto = '<BR> DESTRUTOR ARESTA ' . $this->origem->getIdVertice() . '->'. $this->destino->getIdVertice() .'<BR>';
        // echo $texto;
    }
    public function getOrigem()
    {
        return $this->origem;
    }
    public function getDestino()
    {
        return $this->destino;
    }
    public function getPeso()
    {
        return $this->peso;
    }
    public function setPeso($peso)
    {
        $this->peso = $peso;
    }
    public function getDirecao()
    {
        return $this->direcao;
    }
    public function setDirecao($direcao)
    {
        $this->direcao = $direcao;
    }
    public function print()
    {
        $texto = "------------------------------------- <br>"; 
        $texto .= "ARESTA " . $this->origem->getIdVertice() . " -> " . $this->destino->getIdVertice() . "<br>";
        $texto .= "PESO " . $this->peso . "<br>"; 
        $texto .= "DIRECAO " . $this->direcao . "<br>"; 
        $texto .= "------------------------------------- <br>"; 
        echo $texto;
    }
}
?>
