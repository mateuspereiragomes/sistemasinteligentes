<?php
include_once 'Aresta.class.php';

//classe que contém os elementos de um vértice
class Vertice 
{   
    private $id;
    private $isVisitado;
    private $predecessor; 
    private $posterior;
    private $x;
    private $y;
    private $custo;
    private $direcao; 

    public function __construct($id)
    {
        $this->id = $id;
        $this->isVisitado = false; 
        $this->predecessor = []; 
        $this->posterior = []; 
        $this->x = false; 
        $this->y = false; 
        $this->custo = false; 
        $this->direcao = false; 
    }
    public function __destruct()
    {
        $texto = '<BR> DESTRUTOR VERTICE ' . $this->id . '<BR>';
        // echo $texto;
    }

    public function getIdVertice()
    {
        return $this->id; 
    }

    public function getIsVisitado()
    {
        return $this->isVisitado;
    }

    public function setIsVisitado()
    {
        $this->isVisitado = true;
    }

    public function getPredecessor()
    {
        return $this->predecessor;
    }

    public function setPredecessor($predecessor)
    {
        array_push($this->predecessor, $predecessor);
    }

    public function getPosterior()
    {
        return $this->posterior;
    }

    public function setPosterior($posterior)
    {
        array_push($this->posterior, $posterior);
    }

    public function getX()
    {
        return $this->x;
    }

    public function setX($x)
    {
        $this->x = $x;
    }

    public function getY()
    {
        return $this->y;
    }

    public function setY($y)
    {
        $this->y = $y;
    }

    public function getCusto()
    {
        return $this->custo; 
    }

    public function setCusto($custo)
    {
        $this->custo = $custo; 
    }

    public function setDirecao($direcao)
    {
        $this->direcao = $direcao;
    }
    public function getDirecao()
    {
        return $this->direcao;
    }
    public function print()
    {
        $texto = "------------------------------------- <br>"; 
        $texto .= "VERTICE " . $this->id . "<br>";
        $texto .= "VISITADO " . $this->isVisitado . "<br>"; 
        $texto .= "PREDECESSOR " . implode("|", $this->predecessor) . "<br>"; 
        $texto .= "POSTERIOR " .implode("|", $this->posterior). "<br>"; 
        $texto .= "X " . $this->x . " Y " . $this->y . "<br>"; 
        $texto .= "CUSTO " . $this->custo . "<br>"; 
        $texto .= "DIRECAO " . $this->direcao . "<br>"; 
        $texto .= "------------------------------------- <br>"; 
        echo $texto;
    }
}
?>
