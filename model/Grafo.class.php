<?php
include_once 'Vertice.class.php';
include_once 'Aresta.class.php';
include_once 'InvertedPriorityQueue.class.php';

//classe que contém a estruturação de um grafo assim como as buscas e exibições necessárias
class Grafo 
{   
    //informa se o grafo é direcionado
    public $direcionado; 

    //contém os objetos vértices 
    public $lista_Vertices;

    //contém os objetos arestas
    public $lista_Arestas; 

    //armazena o angulo da direção do agente
    public $direcao;

    public function __construct($direcionado=false)
    {
        $this->direcionado = $direcionado;
        $this->lista_Arestas = array(); 
        $this->lista_Vertices = array();
        $this->direcao = false;
        $this->direcao = false;
        
    }
    public function __destruct()
    {
        $texto = '<BR> DESTRUTOR GRAFO  <BR>';
        // echo $texto;
    }
    public function setDirecao($direcao)
    {
        $this->direcao = $direcao;
    }

    //obtém um vértice através de um id
    public function getVertice($id_vertice)
    {
        foreach ($this->lista_Vertices as $key => $vertice) 
        { 
            if ($vertice->getIdVertice() == $id_vertice) 
            {
                return $vertice;
            }
        }
        return false;
    }

    //diz se um vértice já existe no grafo
    public function getVerticeExistente($id)
    {
        foreach ($this->lista_Vertices as $key => $vertice) 
        { 
            if ($vertice->getIdVertice() == $id) 
            {
                return true;
            }
        }
        return false;
    }

    //diz se uma aresta já existe no grafo 
    public function getArestaExistente($origem, $destino)
    {   
        foreach ($this->lista_Arestas as $key => $aresta)
        {
            if($aresta->getOrigem()->getIdVertice() == $origem->getIdVertice() && $aresta->getDestino()->getIdVertice() == $destino->getIdVertice()) 
            {
                return true;
            }
        }
        return false; 
    }

    //adiciona um vértice ao grafo 
    public function setVertice($vertice)
    {
        $id = $vertice->getIdVertice(); 

        //se não existe adiciona no grafo 
        if(!($this->getVerticeExistente($id)))
        {
            array_push($this->lista_Vertices, $vertice);
        }
    }

    //adiciona uma aresta no grafo 
    public function setAresta($aresta)
    {
        $origem = $aresta->getOrigem();
        $destino = $aresta->getDestino();

        //se ela não existe no grafo 
        if(!($this->getArestaExistente($origem, $destino)))
        {
            array_push($this->lista_Arestas, $aresta);
        }
    }

    //obtém id dos vértices cadastrados no grafo
    public function getVertices()
    {
        $vertices = [];

        foreach($this->lista_Vertices as $key => $vertice) 
        { 
            $id = $vertice->getIdVertice();
            array_push($vertices,$id);  
            // $vertice->print();
        }
        return $vertices;
    }

    //obtém os ids dos vértices de uma aresta
    public function getArestas()
    {
        $arestas = [];

        foreach ($this->lista_Arestas as $key => $aresta) 
        {
            $relacao = [2];
            $relacao[0] = $aresta->getOrigem()->getIdVertice();
            $relacao[1] = $aresta->getDestino()->getIdVertice();
            array_push($arestas, $relacao);
            // $aresta->print();
        }   
        return $arestas;
    }

    //busca em profundidade não recursiva com pesos de direção e aresta
    public function DFSNormal($id_inicial, $id_destino)
    {
        //embaralha as arestas para obter caminhos diferentes
        shuffle($this->lista_Arestas);

        //define o caso inicial 
        $vertice = $this->busca_Vertice($id_inicial);
        $vertice->setPredecessor(-1);
        $vertice->setDirecao($this->direcao);
        $vertice->setCusto(0);

        //adiciona o inicio na pilah
        $pilha = [$id_inicial];

        //enquanto a pilha não for vazia
        while (sizeof($pilha) > 0) 
        {
            //obtém o topo da pilha
            $id_topo = array_pop($pilha);

            // $id_topo = array_shift($pilha);
            
            //para se for encontrar o destino
            if($id_topo == $id_destino)
            {
                break;
            }
            //obtém objeto
            $topo = $this->busca_Vertice($id_topo);

            //obtém adjacentes ao topo 
            $id_adjacentes = $this->busca_Adjacente($id_topo);

            //percorre os adjacente ao topo
            foreach ($id_adjacentes as $id_adjacente) 
            {
                //obtém objeto aresta 
                $aresta = $this->getAresta($id_topo, $id_adjacente);

                //calcula o custo da rotação entre o ângulo do estado do vértice topo e o ângulo da aresta com o adjacente
                $pesoRotacao = $this->calculaPesoRotacao($aresta->getDirecao(), $topo->getDirecao());

                //custo que o topo já possuia + custo da rotacao + custo da aresta
                $custo = $topo->getCusto() + $pesoRotacao + $aresta->getPeso(); 

                //obtém objeto
                $adjacente = $this->busca_Vertice($id_adjacente);

                //verifica se não foi visitado ou se o custo é menor do que o cadastrado no vértice
                if(!$adjacente->getIsVisitado() || ($custo < $adjacente->getCusto())) 
                {
                    //marca como visitado
                    $adjacente->setIsVisitado();

                    //adiciona topo como pai
                    $adjacente->setPredecessor($id_topo);

                    //adiciona o custo para este estado do vértice
                    $adjacente->setCusto($custo);

                    //adiciona a direcao que o vértice adjacente irá 
                    $adjacente->setDirecao($aresta->getDirecao());

                    //adiciona como posterior ao topo
                    $topo->setPosterior($id_adjacente);

                    //adiciona na pilha
                    array_push($pilha, $id_adjacente);
                }
            }
        }
    }
     //busca em largura não recursiva com pesos de direção e aresta
     public function BFSNormal($id_inicial, $id_destino)
     {
         //embaralha as arestas para obter caminhos diferentes
         shuffle($this->lista_Arestas);
 
         //define o caso inicial 
         $vertice = $this->busca_Vertice($id_inicial);
         $vertice->setPredecessor(-1);
         $vertice->setDirecao($this->direcao);
         $vertice->setCusto(0);
 
         //adiciona o inicio na fila
         $fila = [$id_inicial];
 
         //enquanto a fila não for vazia
         while (sizeof($fila) > 0) 
         {
            //obtém o primeiro da fila
             $id_topo = array_shift($fila);
             
             //para se for encontrar o destino
             if($id_topo == $id_destino)
             {
                 break;
             }
             //obtém objeto
             $topo = $this->busca_Vertice($id_topo);
 
             //obtém adjacentes ao topo 
             $id_adjacentes = $this->busca_Adjacente($id_topo);
 
             //percorre os adjacente ao topo
             foreach ($id_adjacentes as $id_adjacente) 
             {
                 //obtém objeto aresta 
                 $aresta = $this->getAresta($id_topo, $id_adjacente);
 
                 //calcula o custo da rotação entre o ângulo do estado do vértice topo e o ângulo da aresta com o adjacente
                 $pesoRotacao = $this->calculaPesoRotacao($aresta->getDirecao(), $topo->getDirecao());
 
                 //custo que o topo já possuia + custo da rotacao + custo da aresta
                 $custo = $topo->getCusto() + $pesoRotacao + $aresta->getPeso(); 
 
                 //obtém objeto
                 $adjacente = $this->busca_Vertice($id_adjacente);
 
                 //verifica se não foi visitado ou se o custo é menor do que o cadastrado no vértice
                 if(!$adjacente->getIsVisitado() || ($custo < $adjacente->getCusto())) 
                 {
                     //marca como visitado
                     $adjacente->setIsVisitado();
 
                     //adiciona topo como pai
                     $adjacente->setPredecessor($id_topo);
 
                     //adiciona o custo para este estado do vértice
                     $adjacente->setCusto($custo);
 
                     //adiciona a direcao que o vértice adjacente irá 
                     $adjacente->setDirecao($aresta->getDirecao());
 
                     //adiciona como posterior ao topo
                     $topo->setPosterior($id_adjacente);
 
                     //adiciona na fila
                     array_push($fila, $id_adjacente);
                 }
             }
         }
     }
    //retorna o objeto vértice relacionado a um id
    public function busca_Vertice($id)
    {
        foreach ($this->lista_Vertices as $key => $vertice) 
        {
            if($vertice->getIdVertice() == $id)
            {
                return $vertice; 
            }
        }
        return false; 
    }

    //percorre recursivamente a árvore geradora e retorna o caminho 
    public function encontra_Caminho($origem, $destino, $path)
    {   
        array_push($path, $destino);

        //finaliza se encontrar o destino
        if($origem == $destino)
        {
            return array_reverse($path);   
        }
        else 
        {
            //obtém objeto
            $destino_aux = $this->busca_Vertice($destino);

            //obtém predecessor ao destino 
            $predecessores = $destino_aux->getPredecessor();

            //verifica se existe predecessor
            if($predecessores == false || $predecessores == NULL || $predecessores == [] || $predecessores[0] == -1)
            {
                return false; 
            }
            else
            {   
                //se existir predecessor chama recursivamente 
                return $this->encontra_Caminho($origem, $predecessores[0], $path); 
            }
        }
    }

    //encontra os adjacentes que ainda não foram visitados
    public function busca_Adjacente($vertice)
    {
        $adjacentes = []; 

        //obtém objeto
        $vertice =  $this->busca_Vertice($vertice);

        //percorre as arestas
        foreach ($this->lista_Arestas as $key => $aresta) 
        {   
            //obtém objetos
            $origem = $aresta->getOrigem();
            $destino = $aresta->getDestino();

            //verifica se é ele
            if($origem->getIdVertice() == $vertice->getIdVertice() && $destino->getIsVisitado() == false)
            {
                //adiciona na lista de adjacentes
                array_push($adjacentes, $destino->getIdVertice());
            }
        }

        return $adjacentes; 
    }

    //heurística euclidiana 
    //função que calcula a distância entre duas posições de vértices 
    public function distanciaEuclidiana($origem, $destino)
    {
        $dx = abs($origem->getX() - $destino->getX());
        $dy = abs($origem->getY() - $destino->getY());

        return sqrt($dx + $dy); 
    }

    //faz a diferença entre dois ângulos e calcula a menor quantidade de passos possíveis 
    public function calculaPesoRotacao($d1, $d2)
    {
        $diferenca = abs($d1 - $d2);

        $voltas = $diferenca/45;

        if($voltas > 4)
        {
            return 8 - $voltas; 
        }
        else 
        {
            return $voltas; 
        }
    }

    //retorna uma aresta entre dois ids de vértices
    public function getAresta($id_origem, $id_destino)
    {
        foreach ($this->lista_Arestas as $key => $aresta) 
        {
            $origem = $aresta->getOrigem();
            $destino = $aresta->getDestino();

            if($origem->getIdVertice() == $id_origem && $destino->getIdVertice() == $id_destino)
            {
                return $aresta;
            }
        }
        return false; 
    }
    // algoritmo A*
    public function A_estrela($id_inicio, $id_fim)
    {   
        //embaralha as arestas para evitar repetir o memso padrão se existir mais de um
        shuffle($this->lista_Arestas);

        //obtém os objetos
        $inicio = $this->busca_Vertice($id_inicio);
        $fim = $this->busca_Vertice($id_fim);

        //fila de prioridade invertida 
        $fila = new InvertedPriorityQueue();
        $fila->insert($id_inicio, 0); 

        //parametros para o vertice inicial
        $inicio->setIsVisitado(); 
        $inicio->setCusto(0);
        $inicio->setPredecessor(-1);
        $inicio->setDirecao($this->direcao);

        //apenas executa enquanto fila não estiver vazia
        while(!$fila->isEmpty())
        {  
            //remove o vértice com menor prioridade do topo da fila 
            $id_atual = $fila->extract();
           
            //se encontrar o destino para
            if($id_atual == $id_fim)
            {
                break; 
            }

            //recebe objeto
            $atual = $this->busca_Vertice($id_atual);

            //obtem os adjacentes que não foram visitados
            $id_adjacentes = $this->busca_Adjacente($id_atual);

            //percorre os adjacentes
            foreach ($id_adjacentes as $id_adjacente)
            {
                //obtém objeto
                $adjacente = $this->busca_Vertice($id_adjacente);

                //obtém aresta
                $aresta = $this->getAresta($id_atual, $id_adjacente);

                //calcula a quantidade pela diferença entre dois ângulos dividido por 45 graus
                $pesoRotacao = $this->calculaPesoRotacao($aresta->getDirecao(), $atual->getDirecao());

                //soma do custo do vértice atual + custo do peso da aresta e da rotacao para ir ao adjacente
                $novo_custo = $atual->getCusto() + $pesoRotacao + $aresta->getPeso(); 
                
                //se não visitado ou se novo custo menor que o custo do adjacente
                if(!$adjacente->getIsVisitado() || ($novo_custo < $adjacente->getCusto() && $adjacente->getCusto() != false))
                {
                    //adiciona custo 
                    $adjacente->setCusto($novo_custo);

                    //adiciona pai
                    $adjacente->setPredecessor($id_atual);
                 
                    //marca visitado
                    $adjacente->setIsVisitado();
                    
                    //adiciona o estado no qual possui o ângulo de direção 
                    $adjacente->setDirecao($aresta->getDirecao());
                   
                    //calcula a heurística
                    $prioridade = $novo_custo + $this->distanciaEuclidiana($fim, $adjacente); 
                    
                    //adiciona adjacente na fila de prioridade 
                    $fila->insert($id_adjacente, $prioridade); 
                }
            }
        }
    }
    //algoritmo dijkstra 
    public function Dijkstra($id_inicio, $id_fim)
    {
        //embaralha as arestas para evitar repetir o memso padrão se existir mais de um
        shuffle($this->lista_Arestas);

        //obtém os objetos
        $inicio = $this->busca_Vertice($id_inicio);
        $fim = $this->busca_Vertice($id_fim);

        //fila de prioridade invertida 
        $fila = new InvertedPriorityQueue();
        $fila->insert($id_inicio, 0); 

        //parametros para o vertice inicial
        $inicio->setIsVisitado(); 
        $inicio->setCusto(0);
        $inicio->setPredecessor(-1);
        $inicio->setDirecao($this->direcao);

        //apenas executa enquanto fila não estiver vazia
        while(!$fila->isEmpty())
        {  
            //remove o vértice com menor prioridade do topo da fila 
            $id_atual = $fila->extract();
           
            //se encontrar o destino para
            if($id_atual == $id_fim)
            {
                break; 
            }

            //recebe objeto
            $atual = $this->busca_Vertice($id_atual);

            //obtem os adjacentes que não foram visitados
            $id_adjacentes = $this->busca_Adjacente($id_atual);

            //percorre os adjacentes
            foreach ($id_adjacentes as $id_adjacente)
            {
                //obtém objeto
                $adjacente = $this->busca_Vertice($id_adjacente);

                //obtém aresta
                $aresta = $this->getAresta($id_atual, $id_adjacente);

                //calcula a quantidade pela diferença entre dois ângulos dividido por 45 graus
                $pesoRotacao = $this->calculaPesoRotacao($aresta->getDirecao(), $atual->getDirecao());

                //soma do custo do vértice atual + custo do peso da aresta e da rotacao para ir ao adjacente
                $novo_custo = $atual->getCusto() + $aresta->getPeso() +  $pesoRotacao; 
                
                //se não visitado ou se novo custo menor que o custo do adjacente
                if(!$adjacente->getIsVisitado() || ($novo_custo < $adjacente->getCusto() && $adjacente->getCusto() != false))
                {
                    //adiciona custo 
                    $adjacente->setCusto($novo_custo);

                    //adiciona pai
                    $adjacente->setPredecessor($id_atual);
                 
                    //marca visitado
                    $adjacente->setIsVisitado();
                    
                    //adiciona o estado no qual possui o ângulo de direção 
                    $adjacente->setDirecao($aresta->getDirecao());
                   
                    //prioridade é apenas o custo
                    $prioridade = $novo_custo;
                    
                    //adiciona adjacente na fila de prioridade 
                    $fila->insert($id_adjacente, $prioridade); 
                }
            }
        }
    }

    //retorna os vértices que foram marcados como visitados pelas buscas
    public function getVerticesVisitados()
    {
        $visitados = [];

        foreach ($this->lista_Vertices as $key => $vertice) 
        {
            if($vertice->getIsVisitado())
            {
                array_push($visitados, $vertice->getIdVertice());
            }
        }
        return $visitados; 
    }
    public function converterMatrizParaGrafo($matriz)
    {
        //valores para decriptografar a matriz
        $verde = 0;
        $vermelho = 1;
        $preto = 2;
        $branco = 3;

        $inicio = false;
        $fim = false; 

        //identifica altura e largura
        $altura = sizeof($matriz);
        $largura = sizeof($matriz[0]);

        //percorre a matriz
        for ($i = 0; $i < $altura; $i++) 
        { 
            for ($j = 0; $j < $largura ; $j++) 
            {   
                //se for diferente de preto(parede)
                if($matriz[$i][$j] == $branco || $matriz[$i][$j]== $vermelho || $matriz[$i][$j] == $verde)
                {
                    // converte x e y em uma label para cada vértice
                    $verificado = $i*$largura + $j + 1;
                    
                    //adiciona o vértice no grafo
                    $vertice_verificado = new Vertice($verificado);
                    $vertice_verificado->setX($j);
                    $vertice_verificado->setY($i);
                    $this->setVertice($vertice_verificado); 
                    
                    //identifica posicao inicial 
                    if($matriz[$i][$j] == $verde)
                    {
                        $inicio = $verificado;
                    }

                    //identifica a posicao final
                    if($matriz[$i][$j] == $vermelho)
                    {
                        $fim = $verificado;
                    }
                    //percorrre os 8 vizinhos de cada vértice
                    for ($y = $i - 1; $y <= $i + 1 ; $y++) 
                    { 
                        for ($x = $j - 1; $x <= $j + 1 ; $x++) 
                        {
                            //se for diferente do centro, ou seja, do vértice verificado
                            $isDiferente = !($i == $y && $j == $x); 

                            //se estiver dentro da matriz
                            $isValido = $x >= 0 && $y >= 0 && $x < $largura && $y < $altura && $isDiferente;
                            
                            if ($isValido)
                            {
                                //se o adjacente não for preto (barreira)
                                if($matriz[$y][$x] == $branco || $matriz[$y][$x] == $vermelho || $matriz[$y][$x] == $verde)
                                {
                                    //adiciona vertice no grafo, caso ele já não esteja
                                    $vizinho = $y * $largura + $x + 1;
                                    $vertice_vizinho = new Vertice($vizinho);
                                    $vertice_vizinho->setX($x);
                                    $vertice_vizinho->setY($y);
                                    $this->setVertice($vertice_vizinho); 
                                    
                                    //cria a arestra
                                    $aresta = new Aresta($vertice_verificado, $vertice_vizinho);

                                    //calcula o peso e a direcao de cada aresta a partir do vertice verificado
                                    if($y == $i - 1 && $x == $j - 1)
                                    {
                                        $aresta->setDirecao(135);
                                        $aresta->setPeso(1.5);
                                    }
                                    if($y == $i - 1 && $x == $j)
                                    {
                                        $aresta->setDirecao(90);
                                        $aresta->setPeso(1);
                                    }
                                    if($y == $i - 1 && $x == $j + 1)
                                    {
                                        $aresta->setDirecao(45);
                                        $aresta->setPeso(1.5);
                                    }
                                    if($y == $i  && $x == $j - 1)
                                    {
                                        $aresta->setDirecao(180);
                                        $aresta->setPeso(1);
                                    }
                                    if($y == $i  && $x == $j + 1)
                                    {
                                        $aresta->setDirecao(0);
                                        $aresta->setPeso(1);
                                    }
                                    if($y == $i + 1 && $x == $j - 1)
                                    {
                                        $aresta->setDirecao(225);
                                        $aresta->setPeso(1.5);
                                    }
                                    if($y == $i + 1 && $x == $j)
                                    {
                                        $aresta->setDirecao(270);
                                        $aresta->setPeso(1);
                                    }
                                    if($y == $i + 1 && $x == $j + 1)
                                    {
                                        $aresta->setDirecao(315);
                                        $aresta->setPeso(1.5);
                                    }

                                    //adiciona a aresta no grafo
                                    $this->setAresta($aresta);
                                }
                            }
                        }
                    }
                }
            }
        }
        
        $posicoes[0] = $inicio;
        $posicoes[1] = $fim;

        return $posicoes;
    }
}
?>
